#!/bin/bash

OPTIND=1

REGION="eu-central-1"
EXECUTION_ROLE_ARN=""
RUNNER_TOKEN=""
GITLAB_URL="https://gitlab.com"
TIMEOUT=300

while getopts "h?r:t:u:a:o:" opt; do
    case "$opt" in
    h|\?)
        echo "usage: ${0} [options]"
        echo "e.g. ${0} -r eu-central-1 -a arn:aws:iam::XXXXXXXXXXXX:role/lambda_execution_role -t xyz123 -u https://gitlab.com -o 300"
        echo ""
        echo "OPTIONS:"
        echo "  -r <REGION> (Region where the gitlab-lambda-runner will be deployed to)"
        echo "  -a <LAMBDA_EXECUTION_ROLE_ARN> (IAM role which will be assigned to the gitlab-lambda-runner)"
        echo "  -t <RUNNER_TOKEN> (Runner token which should be used by the runner to connect to gitlab)"
        echo "  -u <GITLAB_URL> (URL of the gitlab instance where the runner should connect to)"
        echo "  -o <TIMEOUT> (Timeout in seconds for the lambda function, which executes the gitlab-lambda-runner)"
        exit 0
        ;;
    r)  REGION=$OPTARG
        ;;
    t)  RUNNER_TOKEN=$OPTARG
        ;;
    u)  GITLAB_URL=$OPTARG
        ;;
    a)  EXECUTION_ROLE_ARN=$OPTARG
        ;;
    o)  TIMEOUT=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

echo $EXECUTION_ROLE_ARN

if [ "${EXECUTION_ROLE_ARN}" == "" ]; then
    echo "Please specify an EXECUTION_ROLE_ARN, e.g. -a arn:aws:iam::XXXXXXXXXXXX:role/lambda_execution_role"
    exit 1
fi

if [ "${RUNNER_TOKEN}" == "" ]; then
    echo "Please specify a RUNNER_TOKEN, e.g. -t xyz123"
    exit 1
fi

# build layers
docker build -t layer-gitlab-runner:latest -f layers/Dockerfile .

# deploy layers
img2lambda -i layer-gitlab-runner:latest -r $REGION -cr go1.x

# create new final_layers.json which includes the git layer from https://github.com/lambci/git-lambda-layer
cp output/layers.json /tmp/final_layers.json
awk -v n=2 -v s="  \"arn:aws:lambda:${REGION}:553035198032:layer:git:3\"," 'NR == n {print s} {print}' output/layers.json > /tmp/final_layers.json

# deploy lambda
docker run --rm -v "$PWD/":/lambda -e GOOS=linux -e GOARCH=amd64 -w /lambda golang:latest go build -v -o lambda *.go
zip /tmp/lambda.zip lambda

aws lambda create-function \
    --function-name gitlab-lambda-runner \
    --handler lambda \
    --zip-file fileb:///tmp/lambda.zip \
    --runtime go1.x \
    --role $EXECUTION_ROLE_ARN \
    --region $REGION \
    --layers file:///tmp/final_layers.json \
    --environment "Variables={GITLAB_URL=${GITLAB_URL},RUNNER_TOKEN=${RUNNER_TOKEN}}" \
    --timeout $TIMEOUT
